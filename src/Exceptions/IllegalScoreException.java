package Exceptions;

public class IllegalScoreException extends Exception{
    public IllegalScoreException(){
        super();
    }
    
    public IllegalScoreException(String message){
        super(message);
    }
}

