package Exceptions;

public class IllegalSetException extends Exception {
    public IllegalSetException(){
        super();
    }
    
    public IllegalSetException(String message){
        super(message);
    }
}
