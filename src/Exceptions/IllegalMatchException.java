package Exceptions;

public class IllegalMatchException extends Exception{
    public IllegalMatchException(){
        super();
    }
    
    public IllegalMatchException(String message){
        super(message);
    }
}