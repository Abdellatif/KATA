package IndividualSport;

public class Score {
    private int score;
    private boolean advantage;
    
    public Score(int score, boolean advantage){
        this.score = score;
        this.advantage = advantage;
    }
    
    public Score(){
        this.score = 0;
        this.advantage = false;
    }
    
    public int getScore(){
        return this.score;
    }
    
    public void setScore(int score){
        this.score = score;
    }
    
    public boolean isAdvantage(){
        return this.advantage;
    }
    
    public void setAdvantage(boolean advantage){
        this.advantage = advantage;
    }
    
}
