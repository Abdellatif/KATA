package IndividualSport;

import Exceptions.IllegalGameException;
import Exceptions.IllegalMatchException;
import Exceptions.IllegalScoreException;
import Exceptions.IllegalSetException;

public abstract class Round {
    protected Player player1, player2, winner;
    protected Score p1Score, p2Score;
    protected boolean done, exceptionalRole;
    
    public Round(){
        this(new Player(), new Player(), new Score(), new Score(), null, false, false);
    }
    
    public Round(Player player1, Player player2, Score p1Score, Score p2Score, Player winner, boolean exceptionalRole, boolean done){
        this.player1 = player1;
        this.player2 = player2;
        this.winner = winner;
        this.p1Score = p1Score;
        this.p2Score = p2Score;
        this.exceptionalRole = exceptionalRole;
        this.done = done;        
    }
    
    public Round(Player player1, Player player2){
        this(player1, player2, new Score(), new Score(), null, false, false);
    }
    
    public Player getPlayer1(){
        return this.player1;
    }
    
    public void setPlayer1(Player player1){
        this.player1 = player1;
    }
    
    public Player getPlayer2(){
        return this.player2;
    }
    
    public void setPlayer2(Player player2){
        this.player1 = player2;
    }
    
    public Player getWinner(){
        return this.winner;
    }
    
    public void setWinner(Player winner){
        this.winner = winner;
    }
    
    public Score getP1Score(){
        return this.p1Score;
    }
    
    public void setP1Score(Score p1Score){
        this.p1Score = p1Score;
    }
    
    public Score getP2Score(){
        return this.p2Score;
    }
    
    public void setP2Score(Score p2Score){
        this.p2Score = p2Score;
    }
    
    public boolean isDone(){
        return this.done;
    }
    
    public void finishRound(){
        this.done = true;
    }
    
    public boolean isExceptionalRoleActivated(){
        return this.exceptionalRole;
    }
    
    public void setExceptionalRole(boolean exceptionalRole){
        this.exceptionalRole = exceptionalRole;
    }
    
    public abstract void incrementP1Score() throws IllegalScoreException, IllegalSetException , IllegalGameException, IllegalMatchException;
    public abstract void incrementP2Score() throws IllegalScoreException, IllegalSetException , IllegalGameException, IllegalMatchException;
}
