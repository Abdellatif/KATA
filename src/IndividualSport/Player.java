package IndividualSport;

public class Player {
    private String lastName ;
    private String firstName;
    
    public Player(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Player() {
      this.lastName = new String();
      this.firstName = new String();
    }
    
    public String getFirstName(){
        return this.firstName;
    }
    
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    
    public String getLastName(){
        return this.lastName;
    }
    
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    
    @Override
    public boolean equals(Object obj){
        final Player player = (Player)obj;
        return this.firstName.equals(player.getFirstName()) && this.lastName.equals(player.getLastName());
    }
}
