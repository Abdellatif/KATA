package Exceptions;

public class IllegalGameException extends Exception{
    public IllegalGameException(){
        super();
    }
    
    public IllegalGameException(String message){
        super(message);
    }
}
