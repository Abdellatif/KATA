package kata;

import Exceptions.IllegalGameException;
import Exceptions.IllegalMatchException;
import Exceptions.IllegalScoreException;
import Exceptions.IllegalSetException;
import IndividualSport.Player;
import Tennis.SetMatch;

public class KATA {
    public static void main(String[] args) {
    	Player player1 = new Player("Rafael", " Nadal");
    	Player player2 = new Player("Novak", " Djokovic");    	
    	SetMatch setMatch = new SetMatch(player1, player2);

    	try {
    		setMatch.start();
    		KATA.player1Wins(setMatch);
    		System.out.println("--------------");
    		KATA.player1WinsWithADV(setMatch);
    		System.out.println("--------------");
    		KATA.player1WinsWithoutTieBreak(setMatch);
    		System.out.println("--------------");
    		KATA.player1WinsWithTieBreak(setMatch);
    		System.out.println("The winner is : " + setMatch.getWinner().getFirstName() + " " + setMatch.getWinner().getLastName());
    		KATA.player1WinsWithTieBreak(setMatch);
		} catch (IllegalScoreException | IllegalGameException | IllegalMatchException | IllegalSetException e) {
			System.out.println(e.getMessage());
		}
    }
    
    public static void player1Wins(SetMatch setMatch ) throws IllegalScoreException, IllegalGameException, IllegalMatchException, IllegalSetException{
    	//Sprint 1 user story 1
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
		setMatch.showScore();
		setMatch.incrementP1Score();
		setMatch.showScore();

    }
    
    public static void player2Wins(SetMatch setMatch) throws IllegalScoreException, IllegalGameException, IllegalMatchException, IllegalSetException{
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    }
    
    
    public static void player1WinsWithADV(SetMatch setMatch) throws IllegalScoreException, IllegalGameException, IllegalMatchException, IllegalSetException{
		//Sprint 1 user story 2
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP2Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
    	setMatch.showScore();
    	setMatch.incrementP1Score();
    	setMatch.showScore();;
    }
    

    public static void player1WinsWithoutTieBreak(SetMatch setMatch) throws IllegalScoreException, IllegalGameException, IllegalMatchException, IllegalSetException{
    	//Sprint 2
    	KATA.player1Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	KATA.player1Wins(setMatch);
    }
    
    public static void player1WinsWithTieBreak(SetMatch setMatch) throws IllegalScoreException, IllegalGameException, IllegalMatchException, IllegalSetException{
    	//sprint 2 with tie-break
    	KATA.player2Wins(setMatch);
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player1Wins(setMatch);
    	
    	KATA.player2Wins(setMatch);
    	KATA.player2Wins(setMatch);
    	
    	KATA.player1Wins(setMatch);
    }
}