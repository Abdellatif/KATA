package Tennis;

import IndividualSport.Player;
import Exceptions.IllegalGameException;
import Exceptions.IllegalMatchException;
import Exceptions.IllegalScoreException;
import Exceptions.IllegalSetException;
import IndividualSport.Round;
import IndividualSport.Score;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SetMatch extends Round{
	//in this case the exceptional role is the advantage
   List<Match> matchs;
   public SetMatch(Player player1, Player player2, Score p1Score, Score p2Score, Player winner, List<Match> matchs, boolean done){
        super(player1, player2, p1Score, p2Score, winner, false, done);
        this.matchs = matchs;
    }
    
    public SetMatch(){
        super();
        this.matchs = new LinkedList<Match>();
    }
    
    public SetMatch(Player player1, Player player2){
        super(player1, player2);
        this.matchs = new LinkedList<Match>();
    }
    
    public List<Match> getMatchs(){
        return this.matchs;
    }
    
    public void setMatchs(List<Match> matchs){
        this.matchs = matchs;
    }
    
    /**
     * Start a set 
     * @param 
     * @return
     * @throws IllegalMatchException
     * @throws IllegalSetException 
     * @throws IllegalGameException
     */
    public void start() throws IllegalMatchException, IllegalSetException, IllegalGameException{
    	this.addNewMatch();
    	this.currentMatch().addNewGame();
    }
    
    /**
     * Increment Player1 score 
     * @param 
     * @return
     * @throwsIllegalScoreException
     * @throws IllegalSetException 
     * @throwsIllegalGameException
     * @throws IllegalMatchException  
     */
     @Override
     public void incrementP1Score() throws IllegalScoreException, IllegalSetException, IllegalGameException, IllegalMatchException {
    	if(this.isDone())
    		throw new IllegalSetException("Cannot add another match, the current setMatch is already over");
    	
    	//if the current match is over
    	if(this.currentMatch().isDone()){
    		this.addNewMatch().addNewGame().incrementP1Score();
    		this.p1Score.setScore(this.p1Score.getScore() + 1);
    		this.checkToFinishGame();
    		return;
    	}
    	//if the current game is over
    	if(this.currentMatch().currentGame().isDone()){
    		this.currentMatch().addNewGame().incrementP1Score();
    		return;
    	}
    	//if the current game is not over
    	this.currentMatch().currentGame().incrementP1Score();
    	if(this.currentMatch().currentGame().isDone())
    		this.currentMatch().incrementP1Score();
    	
    }
    
     /**
      * Increment Player1 score 
      * @param 
      * @return
      * @throws IllegalScoreException
      * @throws IllegalSetException 
      * @throws IllegalGameException
      * @throws IllegalMatchException 
      */
      @Override
      public void incrementP2Score() throws IllegalScoreException, IllegalSetException, IllegalGameException, IllegalMatchException {
    	if(this.isDone())
    		throw new IllegalSetException("Cannot add another match, the current setMatch is already over");
    	
    	//if the current match is over
    	if(this.currentMatch().isDone()){
    		this.addNewMatch().addNewGame().incrementP2Score();
    		this.p2Score.setScore(this.p2Score.getScore() + 1);
    		this.checkToFinishGame();
    		return;
    	}
    	//if the current game is over, we create another game
    	if(this.currentMatch().currentGame().isDone()){
    		this.currentMatch().addNewGame().incrementP2Score();
    		return;
    	}
    	//if the current game is not over
    	this.currentMatch().currentGame().incrementP2Score();
    	if(this.currentMatch().currentGame().isDone())
    		this.currentMatch().incrementP2Score();
    }
    
    /**
     * To add a new match
     * @param 
     * @return The new match
     * @throws IllegalMatchException
     * @throws IllegalSetException
     */
    public Match addNewMatch() throws IllegalMatchException, IllegalSetException{
        if((this.currentMatch() != null && !this.currentMatch().isDone()))
            throw new IllegalMatchException("Cannot add another match, the current match is in progress");
        if(this.isDone())
            throw new IllegalSetException("Cannot add another match, the current setMatch is already over");
        this.matchs.add(new Match(player1, player2));
        return this.matchs.get(this.matchs.size()-1);
    }
    
    /**
     * The current match
     * @param 
     * @return The last game
     * @throws IllegalGameException
     */
    public Match currentMatch(){
        return this.matchs.isEmpty() ? null : this.matchs.get(this.matchs.size()-1);
    }
    
    /**
     * Check if the constraints are reached to finish the set :
     * 		: The first player has score (2) wins    
     * @param 
     * @return 
     */
    public void checkToFinishGame(){
    	
    	if(p1Score.getScore() >= 2){
    		this.setWinner(player1);
    		this.finishRound();
    		return;
    	}
    	if(p2Score.getScore() >= 2){
    		this.setWinner(player2);
    		this.finishRound();
    	}
    	
    }
    
    /**
     * The display the setMatch
     * @param 
     * @return
     */
    public void showScore(){
    	String advP1 = new String();
        String advP2 = new String(); 
        if(this.currentMatch().currentGame().getP1Score().isAdvantage()== true){
            advP1 = " (AD) ";
        }
        if(this.currentMatch().currentGame().getP2Score().isAdvantage()== true){
            advP2 = " (AD) ";
        } 
        StringBuffer p1Score = new StringBuffer("\t" + this.player1.getFirstName() + " " + this.player1.getLastName()+ advP1 + " : ");
        StringBuffer p2Score = new StringBuffer("\t" + this.player2.getFirstName() + " " + this.player2.getLastName()+ advP2+" : ");
        
        p1Score.append(this.currentMatch().currentGame().getP1Score().getScore()+" -> ");
        p2Score.append(this.currentMatch().currentGame().getP2Score().getScore()+" -> ");
        
        for(Iterator it = this.getMatchs().iterator(); it.hasNext();){
            Match match = (Match)it.next();
            p1Score.append(match.getP1Score().getScore()+" ");
            p2Score.append(match.getP2Score().getScore()+" ");
        }
        System.out.println(p1Score);
        System.out.println(p2Score);
        if(this.isDone())
        	System.out.println("The winner is : "+ this .winner.getFirstName() +" "+ this.winner.getLastName());
    }
}

