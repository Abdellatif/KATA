package Tennis;

import IndividualSport.Player;
import Exceptions.IllegalGameException;
import Exceptions.IllegalMatchException;
import Exceptions.IllegalScoreException;
import IndividualSport.Round;
import IndividualSport.Score;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Match extends Round{
    //in this case the exceptional role is the tie-break
    private List<Game> games;
    
    public Match(Player player1, Player player2, Score p1Score, Score p2Score, Player winner, List<Game> games, boolean tieBreak, boolean done){
        super(player1, player2, p1Score, p2Score, winner, tieBreak, done);
        this.games = games;
    }
    
    public Match(){
        super();
        this.games = new LinkedList<Game>();
    }
    
    public Match(Player player1, Player player2){
        super(player1, player2);
        this.games = new LinkedList<Game>();
    }
    
    public List<Game> getGames(){
        return this.games;
    }
    
    public void setGames(List<Game> games){
        this.games = games;
    }
    
    /**
    * Increment Player1 score 
    * @param 
    * @return
    */
    @Override
    public void incrementP1Score() throws IllegalScoreException {
        this.incrementPlayerScore(this.player1, this.p1Score, this.p2Score);
    }
    
    /**
     * Increment Player2 score 
     * @param 
     * @return
     */
    @Override
    public void incrementP2Score() throws IllegalScoreException {
        this.incrementPlayerScore(this.player2,this.p2Score, this.p1Score);
    }
    
    /**
    * Increment a Player score 
    * @param{Player} targetPlayer : The owner of the score to increment
    * @param{Score} targetScore : The score to increment
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    * @throws IllegalScoreException
    */
    public void incrementPlayerScore(Player targetPlayer, Score targetScore, Score concurentScore) throws IllegalScoreException{
        if(this.isDone())
            throw new IllegalScoreException("Cannot increment scores, the current match is already over");
        //this.simpleRule(targetPlayer, targetScore, concurentScore);
        this.advancedRule(targetPlayer, targetScore, concurentScore);
    }
    
    /**
    * To manage the simple role 
    * 		: If the targetPlayer reaches the score (6) and his adversary has 4 or lower as score, the targetPlayer wins
-          	: If the targetPlayer reaches the score (6) and the his adversary has a score (5), a new Game must be played
 			: If the targetPlayer reach the score (7), he wins the match
    * @param{Player} targetPlayer : The player who wins the point 
    * @param{Score} targetScore : The score the targetPlayer
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    */
    private void simpleRule(Player targetPlayer, Score targetScore, Score concurentScore){
        targetScore.setScore(targetScore.getScore() + 1);
        if((targetScore.getScore() == 6 && concurentScore.getScore() <=4)
                || targetScore.getScore() == 7) {
            this.finishRound();
            this.setWinner(targetPlayer);
        }
    }
    
    /**
    * To manage the advanced role
    * 		: If the targetPlayer reaches the score (6) and his adversary has 4 or lower as score, the targetPlayer wins the round
-          	: If the two players reaches the score (6) the tie break rule is activated
 			: If the targetPlayer reach the score (7), and his adversary has the score 5, the targetPlayer wins the round
    * @param{Player} targetPlayer : The player who wins the point 
    * @param{Score} targetScore : The score the targetPlayer
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    */
    private void advancedRule(Player targetPlayer, Score targetScore, Score concurentScore){
        targetScore.setScore(targetScore.getScore()+ 1);
        if(this.isExceptionalRoleActivated() 
            ||(targetScore.getScore() >= 6 && concurentScore.getScore() >= 6)){
            this.tieBreakCase(targetPlayer, targetScore, concurentScore);
            return;
        }
        if((targetScore.getScore() == 6 && concurentScore.getScore() <=4)
            ||(targetScore.getScore() == 7 && concurentScore.getScore() == 5)
                || (targetScore.getScore() > 6)){
            this.finishRound();
            this.setWinner(targetPlayer);
        }
    }
    
    /**
     * To manage the tie-break rule
     * 		: If the targetPlayer has two points more than his adversary, he wins the round
     * @param{Player} targetPlayer : The player who won the point 
     * @param{Score} targetScore : The score the targetPlayer
     * @param{Score} concurentScore : The score of the targetPlayer adversary
     * @return
     */
    private void tieBreakCase(Player targetPlayer, Score targetScore, Score concurentScore){
        this.setExceptionalRole(true);
        if(targetScore.getScore() == (concurentScore.getScore() + 2)){
            this.finishRound();
            this.setWinner(targetPlayer);
        }
    }
    
    /**
     * To add a new game
     * @param 
     * @return The new game
     * @throws IllegalGameException
     */
    public Game addNewGame() throws IllegalGameException, IllegalMatchException{
        if((this.currentGame() != null && !this.currentGame().isDone()) || this.isDone())
            throw new IllegalGameException("Cannot add another game, the current game is in progress");
        if(this.isDone())
            throw new IllegalMatchException("Cannot add another game, the current match is already over");
        this.games.add(new Game(player1, player2));
        return this.games.get(this.games.size()-1);
    }
    
    /**
     * The current game
     * @param 
     * @return The last game
     */
    public Game currentGame(){
        return this.games.isEmpty() ? null : this.games.get(this.games.size()-1);
    }
    
    /**
     * The display the match
     * @param 
     * @return
     */
    public void showScore(){
        String advP1 = new String();
        String advP2 = new String(); 
        if(currentGame().getP1Score().isAdvantage()== true){
            advP1 = " (AD) ";
        }
        if(currentGame().getP2Score().isAdvantage()== true){
            advP2 = " (AD) ";
        } 
        StringBuffer p1Score = new StringBuffer("\t" + player1.getFirstName() + " " +player1.getLastName()+ advP1 + " : ");
        StringBuffer p2Score = new StringBuffer("\t" + player2.getFirstName() + " " +player2.getLastName()+ advP2+" : ");

        for(Iterator it = this.games.iterator(); it.hasNext();){
            Game game = (Game)it.next();
            p1Score.append(game.getP1Score().getScore()+" ");
            p2Score.append(game.getP2Score().getScore()+" ");
        }
        System.out.println(p1Score);
        System.out.println(p2Score);
    }
    
}
