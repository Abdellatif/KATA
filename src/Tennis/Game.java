package Tennis;

import Exceptions.IllegalScoreException;
import IndividualSport.*;

public class Game extends Round{
	
	public Game(){
        super();
    }
    
    public Game(Player player1, Player player2, Score p1Score, Score p2Score, Player winner, boolean deuce, boolean done){
        super(player1, player2, p1Score, p2Score, winner, deuce, done);
    }
    
    public Game(Player player1, Player player2){
        super(player1, player2);
    }
    
    /**
    * Increment Player1 score 
    * @param 
    * @return
    */
    @Override
	public void incrementP1Score() throws IllegalScoreException {
        this.incrementPlayerScore(this.player1, this.p1Score, this.p2Score);
	}
	
    /**
    * Increment Player2 score
    * @param  
    * @return
    */
    @Override
	public void incrementP2Score() throws IllegalScoreException {
		this.incrementPlayerScore(this.player2, this.p2Score, this.p1Score);	
	}
	
    /**
    * Increment a Player score 
    * @param{Player} targetPlayer : The player who won the point
    * @param{Score} targetScore : The score to increment
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    * @throws IllegalGameException
    */
    private void incrementPlayerScore(Player targetPlayer, Score targetScore, Score concurentScore) throws IllegalScoreException{
        if(this.isDone())
            throw new IllegalScoreException("Cannot increment scores, the current game is already over");
        if(targetScore.getScore() == 0){
            targetScore.setScore(15);
            return;
        }
        if(targetScore.getScore() == 15){
            targetScore.setScore(30);
            return;
        }
        if(targetScore.getScore() == 30){
            targetScore.setScore(40);
            if(concurentScore.getScore() == 40)
                this.exceptionalRole= true;
            return;
        }
        if(targetScore.getScore() == 40){
            reachDeuceCase(targetPlayer, targetScore, concurentScore);
        }
    }
    
    /**
    * To manage the deuce rule with advantage
    * @param{Player} targetPlayer : The player who won the point
    * @param{Score} targetScore : The score the targetPlayer
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    */
    private void reachDeuceCase(Player targetPlayer, Score targetScore, Score concurentScore){
        if(this.isExceptionalRoleActivated())
            this.deuceCase(targetPlayer, targetScore, concurentScore);
        else{
            this.finishRound();
            this.p1Score.setScore(0); this.p1Score.setAdvantage(false);
            this.p2Score.setScore(0); this.p2Score.setAdvantage(false);
            this.setWinner(targetPlayer);
        }
    }
    
    /**
    * The deuce role : the targetPlayer may take the advantage or win the round
    * @param{Player} targetPlayer : The player with the advantage or the winner
    * @param{Score} targetScore : The score the targetPlayer
    * @param{Score} concurentScore : The score of the targetPlayer adversary
    * @return
    */
    private void deuceCase(Player targetPlayer, Score targetScore, Score concurentScore){
        if(targetScore.isAdvantage()){
            this.finishRound();
            this.p1Score.setScore(0); this.p1Score.setAdvantage(false);
            this.p2Score.setScore(0); this.p2Score.setAdvantage(false);
            this.setWinner(targetPlayer);
        }
        else{
            targetScore.setAdvantage(true);
            concurentScore.setAdvantage(false);
        }
    }
}
